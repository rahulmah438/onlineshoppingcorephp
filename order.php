<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
session_start();  

include_once("user.php");
include_once("item.php");

$itemno=$_REQUEST['itemno'];
$email=$_SESSION['UserEmail'];
$title = $_SESSION['title'];
$firstname = $_SESSION['firstname'];
$lastname = $_SESSION['lastname'];



if($_REQUEST['order'])
  {
  $pname=$_REQUEST['product'];
  $itemno=$_REQUEST['item'];
  $price=$_REQUEST['price'];
  $size=$_REQUEST['size'];
  $uname=$_REQUEST['username'];
  $ac_no=$_REQUEST['account'];
  $mob_no=$_REQUEST['mobile'];
  $add=$_REQUEST['address'];
  $bank=$_REQUEST['bank'];
  $city=$_REQUEST['city'];
  $order_no=ord.rand(100,500);
 
  $item->placeOrder($pname, $itemno, $price, $size, $uname, $ac_no, $mob_no, $add, $bank, $city, $order_no);
  
}
	
if($_REQUEST['log']=='out') {
    session_destroy();
    header("location:index.php");
}
else if($email=="") {
    header("location:index.php");
}
	
?>
<!DOCTYPE html>
  <head>
  <link rel="stylesheet" href="css/a.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
  <body>
      <div>
      <div><br/><center><h2><font face="Lucida Handwriting" size="+2" color="#00CCFF">Welcome 
<?php
  echo $title."&nbsp;".$firstname."&nbsp;".$lastname;
?>
      </font></h2></center>
      <h4 margin-left="1000px"><a href="?log=out"><font color="#0099FF">LogOut</font></a></h4>
      </div>
<?php
    $result = $item->getItems($itemno);
    $ItemDetails = mysqli_fetch_assoc($result);
?>



    <div style="width:25%;float:left;">
        <img id="register" src="images/7.jpg">
    </div>
	  <div class="container">
		  <div class="col-md-5" style="margin-left:-190px; float:left;">
        <form method="post">
        <h2 align="center"><font size="+1" color="#00CCFF">Order form</font></h2>
        
          <div class="form-group">
            <label>Product name</label>
            <input type="text" name="product" class="form-control" readonly="readonly" value="<?php echo $ItemDetails['desc'];?>"/>
          </div>
          <div class="form-group">
            <label>Item Number</label>
            <input type="text" name="item" class="form-control" readonly="readonly" value="<?php echo $ItemDetails['itemno'];?>"/>
          </div>
          <div class="form-group">
            <label>Price</label>
            <input type="text" name="price" class="form-control" readonly="readonly" value="Rs <?php echo $ItemDetails['price'];?>"/>
          </div>
          <div class="form-group">
            <label>size</label>
            <select name = "size" class="form-control">
              <option value="Small" selected>Small</option>
              <option value="Medium">Medium</option>
              <option value="Large">Large</option>
              <option value="Xtra Large">Xtra Large</option>
            </select>
          </div>
          <div class="form-group">
            <label>Name</label>
            <input type="text" name="username" class="form-control" />
          </div>
          <div class="form-group">
            <label>Account Number</label>
            <input type="text" name="account" class="form-control" />
          </div>
          <div class="form-group">
            <label>Mobile Number</label>
            <input type="text" name="mobile" maxlength="10" class="form-control" />
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" name="address" class="form-control" />
          </div>
          <div class="form-group">
            <label>Bank</label>
            <select name = "bank" class="form-control">
              <option value="SBBJ">SBBJ</option>
              <option value="SBI" selected>SBI</option>
              <option value="ICICI">ICICI</option>
              <option value="HDFC">HDFC</option>
              <option value="PNB">PNB</option>
              <option value="Axis Bank"> Axis Bank</option>
            </select>
          <div>
          <div class="form-group">
            <label>City</label>
            <input type="text" name="city" class="form-control" />
          </div>
          <div class="form-group" align="center">
            <input type="submit" name="order" class="btn btn-info" value="Submit" />
          </div>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
