-- MySQL dump 10.13  Distrib 5.5.61, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: shop
-- ------------------------------------------------------
-- Server version	5.5.61-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `shop`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `shop` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `shop`;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `cat_id` varchar(30) NOT NULL,
  `category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES ('1','men'),('2','women'),('3','kids');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `details`
--

DROP TABLE IF EXISTS `details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `details` (
  `name` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `details`
--

LOCK TABLES `details` WRITE;
/*!40000 ALTER TABLE `details` DISABLE KEYS */;
INSERT INTO `details` VALUES ('admin','rahul123');
/*!40000 ALTER TABLE `details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `name` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `subj` varchar(30) NOT NULL,
  `mesg` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES ('Rahul','9458485085','rahulmah438@gmail.com','service','Try to improve your service.'),('Rahul','9458485085','rahulmah438@gmail.com','service','Good service');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `category` varchar(40) NOT NULL,
  `subcatg` varchar(40) NOT NULL,
  `itemno` varchar(30) NOT NULL,
  `price` int(10) NOT NULL,
  `desc` varchar(300) NOT NULL,
  `info` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES ('2','Dresses','d4',1899,'Semi Formal Dress','Fabric Detailing Cap Sleeves Open Bust Button'),('2','Dresses','d2',1699,'Casual Dress','Viscose Blend Fabric Smoked Back Smart Shift Dress Sleeveless Linen Shift Dress'),('2','Dresses','d6',1799,'Formal dress','Pin Striped Incut Dress Polyester Fabric Sleeveless  '),('2','Dresses','d7',1649,'Formal Dress','Buttoned Down Front Linen Fabric Sleeveless'),('1','Casual Shirts','cs2',1999,'United Colors of Benetton Mens Full Sleeves Checks Shirt','100% Cotton Full Sleeves Smart Checks Earthy Foliage Combination'),('1','Casual Shirts','cs3',1999,'United Colors of Benetton Mens Full Sleeves Checks Shirt','100% Cotton Full Sleeves Stripe Shirt Light Weight Fabric'),('1','Casual Shirts','cs4',1999,'United Colors of Benetton Mens Full Sleeves Checks Shirt\r\n','United Colors of Benetton Mens Full Sleeves Checks Shirt'),('1','Casual Shirts','cs5',1499,'Allen Solly Mens Full Sleeves Checks Sport Shirt','Bias Yoke Patch Pockets Slim Fit Half Sleeves'),('1','Casual Shirts','cs6',1999,'Levis Mens Roll Up Sleeves Workers Shirt','Light Age Theme 100% Cotton Full Sleeves Checks Shirt White Base'),('1','Casual Shirts','cs0',1599,'Mustang Mens Long Sleeves Mini Checks Shirt','Summer Colors Theme 100% Cotton Full Sleeves Bias Detailing at Side Seam'),('2','Churidar Suits','s5',1699,'BIBA Churidar - Kurta-Dupatta Set','Printed kurta Round neck Full sleeves Kurta length -40 inches Contrast churidar and shaded crushed Duppatta'),('2','Churidar Suits','s1',2599,'Kashish Churidar - Kurta-Dupatta Set','Floral print Embelished Yoke Puff sleeves Kurta length -40 inches Contrast churidar and Duppatta'),('2','Churidar Suits','s3',2899,'Kashish Churidar - Kurta-Dupatta Set','Textured Fabric V-neck 3/4th sleeves Kurta length -40 inches Contrast churidar and Duppatta'),('2','Churidar Suits','s6',2799,'Seven East Churidar - Kurta-Dupatta Set','Textured Fabric Embelished Yoke Sleeveless Kurta length -38 inches Contrast churidar and Duppatta'),('2','Churidar Suits','s4',2799,'Kashish Churidar - Kurta-Dupatta Set','Printed kurta Round neck Half sleeves Kurta length -38 inches Contrast churidar and Printed Duppatta'),('2','Churidar Suits','s2',2299,'BIBA Churidar - Kurta-Dupatta Set','Regular fit Round neck Full sleeves Kurta length -40 inches Contrast churidar and shaded crushed Duppatta   '),('2','Sandals','foot1',1690,'Tresmode Ladies Peep toes','Tresmode Ladies Peep toes'),('2','Sandals','foot2',1690,'Tresmode Ladies Pump Shoes','Tresmode Ladies Peep toes'),('2','Sandals','foot4',4990,'Tresmode Ladies Sandals','Tresmode Ladies Peep toes'),('2','Sandals','nf2',1299,'Haute Curry - Ladies Footwear\r\n','Haute Curry - Ladies Footwear'),('2','Sandals','nf3',1399,'Lemon Pepper - Ladies Footwear\r\n','Lemon Pepper - Ladies Footwear'),('2','Sandals','nf1',1499,'Lemon Pepper - Ladies Footwear\r\n','Lemon Pepper - Ladies Footwear'),('2','Kurtas','k3',1399,'Haute curry Mix and Match Kurta','Tie up 5/8th sleeves Kurta Length - 38 inches Tribal printed 100% cotton  '),('2','Kurtas','k4',1079,'W Mix and match kurta\r\n','Sleeveless Regular Wear Regular Fit Length- 38 inches Fabric- Cotton Cambric'),('2','Kurtas','k2',1599,'Kashish Mix and Match Kurta','3/4th sleeves Kurta Length - 38 inches sequine highlighted printed 100% cotton'),('2','Kurtas','k7',799,'W Mix and Match Kurta','V Neck Kurta Printed Short Sleeves Mauve printed non embellished Kurta Kurta length -38 inches 60 s Cambric'),('2','Kurtas','k8',1199,'Stop Classic Mix and Match Short Kurta\r\n','Dobby kurta with embroidery on the yoke and sleeve Kurta length- 36 inches 3/4th sleeves Officewear'),('2','Kurtas','k9',1299,'Stop Classic Mix and Match Short Kurta\r\n','Printed Highlighted placket and sleeve hem 3/4th sleeves'),('2','Dresses','d5',1799,'Life Ladies Rara Dress\r\n','Life Ladies Rara Dress'),('2','Dresses','d3',1599,'Mustang Ladies Mia Denim dress\r\n','Mustang Ladies Mia Denim dress'),('2','Office Wear','of2',799,'Austin Reed Office Wear T shirt','Austin Reed Office Wear T shirt'),('2','Office Wear','of1',1999,'Austin Reed Office Wear waist coat','Austin Reed Office Wear waist coat'),('2','Office Wear','of5',799,'Austin Reed Office Wear Top','Austin Reed Office Wear Top'),('2','Office Wear','of3',899,'Austin Reed Office Wear T shirt','Austin Reed Office Wear T shirt'),('2','Office Wear','of4',1199,'Austin Reed Office Wear T shirt','Austin Reed Office Wear T shirt'),('2','Office Wear','of6',699,'SHOP Office wear collection','SHOP Office wear collection'),('2','Artificial Jewellery','j7',20,'Haute Curry Earring HCSE1012','Haute Curry Earring HCSE1012'),('2','Artificial Jewellery','j3',10,'Lucera Rhodium plated Sterling Silver Diamond tops ','Lucera Rhodium plated Sterling Silver Diamond tops '),('2','Artificial Jewellery','j4',20,'Pretty Women Peacock theme Set ','Pretty Women Peacock theme Set '),('2','Artificial Jewellery','j2',23,'Pretty Women dangling Earrings ','Pretty Women dangling Earrings '),('2','Artificial Jewellery','j6',22,'DITI Flower theme Diamond Ring ','DITI Flower theme Diamond Ring '),('2','Artificial Jewellery','j5',24,'Pretty Women Modern theme set ','Pretty Women Modern theme set '),('3','Baby Apparel','bb1',899,'Girls Spot and Stripe Jersey Romper Dress','Floral print Sleeveless Round neck Along with belt Prices of the items may be different from those displayed on the product details page, where the price varies by size'),('3','Baby Apparel','bb2',1,'Girls Denim Pinny','Floral print denim dress Sleeveless Round neck Along with a belt Prices of the items may be different from those displayed on the product details page, where the price varies by size'),('3','Baby Apparel','bb7',1,'Girls frill and pompy short Dress','Polka dot Strappy Square neck Double layered dress Prices of the items may be different from those displayed on the product details page, where the price varies by size'),('3','Baby Apparel','bb3',1,'Girls Layette Snowsuit','Girls Layette Snowsuit'),('3','Baby Apparel','bb5',899,'Unisex Duck Pyjamas','Round Neck Half Sleeves Stripes With Print Button Styling Prices of the items may be different from those displayed on the product details page, where the price varies by size.'),('3','Baby Apparel','bb4',1,'Boys Truck Bodysuits - 7pk','Round Neck Half Sleeves Stripes Button Styling Prices of the items may be different from those displayed on the product details page, where the price varies by size.'),('3','Girls Apparel','g1',999,'Gini and Jony girls dress (Infant)',' Sleeveless Balloon dress Striped at the waist Prices of the items may be different from those displayed on the product details page, where the price varies by size.'),('3','Girls Apparel','g2',445,'612 Ivy League girls dress',' Floral print, Strappy, Elasticised at the hem, Prices of the items may be different from those displayed on the product details page, where the price varies by size.'),('3','Girls Apparel','g6',699,'Girls floral dress','Floral print Strappy Elasticized body Frills at the hem Prices of the items may be different from those displayed on the product details page, where the price varies by size.'),('3','Girls Apparel','g3',499,'Shop girls top','Stop girls top Product Details Floral print Halter Bow at the side Prices of the items may be different from those displayed on the product details page, where the price varies by size.'),('3','Girls Apparel','g5',549,'Shop girls skirt',' Floral print Halter neck V-neck Bow at the front Frills at the bottom Prices of the items may be different from those displayed on the product details page, where the price varies by size'),('3','Girls Apparel','g4',549,'Shop girls casual top','Polka dot Puff sleeves Collar Polka dot print belt Front pleat Prices of the items may be different from those displayed on the product details page, where the price varies by size'),('3','Boys Apparel','b1',499,'United Colors of Benetton boys t-shirt','Half sleeves Printed message &#39;St.Kilda surfer&#39; Round neck Regular fit Prices of the items may be different from those displayed on the product details page, where the price varies by size.  '),('3','Boys Apparel','b2',399,'United Colors of Benetton boys t-shirt','Half sleeves Printed message &#39;Art village 2&#39; Round neck Regular fit Prices of the items may be different from those displayed on the product details page, where the price varies by size.'),('3','Boys Apparel','b4',799,'Gini and Jony boys T-shirt (Kids)','Printed tee Half sleeves Polo neck Printed baseball logo Prices of the items may be different from those displayed on the product details page, where the price varies by size'),('3','Boys Apparel','b3',545,'612 Ivy League boys shirt','Checks shirt Half sleeve with turnup loop Along with a tee Printed tee and shirt Prices of the items may be different from those displayed on the product details page, where the price varies by size.'),('3','Boys Apparel','b5',745,'612 Ivy League boys T-shirt','Striped shirt Half sleeves Patch on the front Contrast collar and sleeve hem Prices of the items may be different from those displayed on the product details page, where the price varies by size.'),('3','Boys Apparel','b6',879,'Nike boys t-shirt','Solid color tee Sleeveless Round neck Printed logo \'64\' Prices of the items may be different from those displayed on the product details page, where the price varies by size'),('3','Kids Toys','e2',525,'Wild Republic Rascals Dolphin 20 inch soft toy','Simple Cute fluffy and adorable Non Toxic Quality Fabric Ultra Squishy Stuffing 20 inches Hand Crafted'),('3','Kids Toys','e3',2499,'Little Mommy Play All Day','Doll'),('3','Kids Toys','e4',1649,'Road Storage Mat R','A fantastic backdrop for road adventures and a practical storage case in one Race your toy cars on the playmat then fold the playmat into a storage box and place your favourite toys in it Water resistant wipe-clean surface Age Range: From 3 years Social skills-This toy helps your child learn how to make friends and enjoy company. Imagination-This toy encourages your child to enjoy using their imagination.'),('3','Kids Toys','e5',549,'Paper Straw Pets','Use your imagination to create your very own straw pets and a house for them too Age Range: From 3 years Creativity-This toy enables your child to express themselves artistically. Imagination-This toy encourages your child to enjoy using their imagination.'),('3','Kids Toys','e6',1999,'Disney Rapunzel hair braider','Manually operated Rapunzel doll'),('3','Kids Toys','e1',699,'Paper Aquarium','Have wonderful adventures with these dazzling paper fish and funky fish tank. Age Range: From 3 years Imagination-This toy encourages your child to enjoy using their imagination. Creativity-This toy enables your child to express themselves artistically.'),('1','T-shirts','ts5',1699,'United Colors of Benetton Mens Half Sleeves Polo Striper T-Shirt','United Colors of Benetton Mens Half Sleeves Polo Striper T-Shirt'),('1','T-shirts','ts4',1099,'United Colors of Benetton Mens Half Sleeves Polo T-Shirt1','United Colors of Benetton Mens Half Sleeves Polo T-Shirt1'),('1','T-shirts','ts1',1299,'United Colors of Benetton Mens Half Sleeves Polo T-Shirt','United Colors of Benetton Mens Half Sleeves Polo T-Shirt'),('1','T-shirts','ts3',1099,'Spykar Mens Half Sleeve Collar Neck Flat Knit T-Shirt','Spykar Mens Half Sleeve Collar Neck Flat Knit T-Shirt'),('1','T-shirts','ts6',749,'Spykar Mens Half Sleeve Round Neck Printed T-Shirt','Spykar Mens Half Sleeve Round Neck Printed T-Shirt'),('1','T-shirts','ts2',999,'Mustang Mens Circular Knit Prinetd Short Sleeves T-Shirt','Mustang Mens Circular Knit Prinetd Short Sleeves T-Shirt'),('1','jeans','jeans1',2399,'Mustang Mens New Oregon Fit Denim','Mustang Mens New Oregon Fit Denim'),('1','jeans','jeans4',1699,'Flying Machine Mens Regular Fit Bruce Denim','Flying Machine Mens Regular Fit Bruce Denim'),('1','jeans','jeans5',1999,'United Colors of Benetton Mens Slim Fit Denim','United Colors of Benetton Mens Slim Fit Denim'),('1','jeans','jeans2',2199,'Mustang Mens Michigan Fit Denim','Mustang Mens Michigan Fit Denim'),('1','jeans','jeans3',1599,'Flying Machine Mens Slim Fit Eddie Denim','Flying Machine Mens Slim Fit Eddie Denim'),('1','jeans','jeans6',2599,'Levis Mens 504 Fit Tapered Denim','Levis Mens 504 Fit Tapered Denim'),('1','Footwear','shoe1',3999,'Enroute Men\'s Footwear','Enroute Men\'s Footwear'),('1','Footwear','shoe2',4499,'Enroute Men\'s Footwear','Enroute Men\'s Footwear'),('1','Footwear','shoe4',3999,'Enroute Men\'s Footwear','Enroute Men\'s Footwear'),('1','Footwear','shoe3',1676,'Franco Leone - Men\'s Shoes','Franco Leone - Men\'s Shoes'),('1','Footwear','shoe5',2195,'Franco Leone - Men\'s Shoes','Franco Leone - Men\'s Shoes'),('1','Footwear','shoe6',1895,'Franco Leone - Men\'s Shoes','Franco Leone - Men\'s Shoes'),('1','watches','w1',3650,'Polo Club Men\'s Watch','Polo Club Men\'s Watch'),('1','watches','w3',1750,'Polo Club Men\'s Watch','Polo Club Men\'s Watch'),('1','watches','w4',1750,'Polo Club Men\'s Watch','Polo Club Men\'s Watch'),('1','watches','w5',1569,'Polo Club Men\'s Watch','Polo Club Men\'s Watch'),('1','watches','w6',1999,'Polo Club Men\'s Watch','Polo Club Men\'s Watch'),('1','watches','w2',1349,'Polo Club Men\'s Watch','Polo Club Men\'s Watch'),('1','Shorts','sh1',1799,'Mustang Mens Casual Shorts','Mustang Mens Casual Shorts'),('1','Shorts','sh3',1299,'Killer Mens 8 Pocket Checks Shorts','Killer Mens 8 Pocket Checks Shorts'),('1','Shorts','sh4',1295,'Wrangler Mens Spencer Denim Shorts','Wrangler Mens Spencer Denim Shorts'),('1','Shorts','sh6',1299,'Killer Mens 8 Pocket Checks Shorts','Killer Mens 8 Pocket Checks Shorts'),('1','Shorts','sh5',2195,'Wrangler Mens Cargo Fit Checks Shorts','Wrangler Mens Cargo Fit Checks Shorts'),('1','Shorts','sh2',1699,'Indian Terrain Mens Regular Fit Cargo Shorts','Indian Terrain Mens Regular Fit Cargo Shorts'),('1','Casual Shirts','cs1',5000,'Good','100% Cotton Full Sleeves Smart Checks Earthy Foliage Combination');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `pname` varchar(30) NOT NULL,
  `itemno` varchar(30) NOT NULL,
  `price` varchar(30) NOT NULL,
  `size` varchar(30) NOT NULL,
  `uname` varchar(30) NOT NULL,
  `ac_no` varchar(30) NOT NULL,
  `mob_no` varchar(30) NOT NULL,
  `add` varchar(300) NOT NULL,
  `bank` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `order_no` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES ('W Mix and match kurta','k4','Rs 1079','Small','rahul','33383684568','9458485085','Kasganj','SBI','Kasganj','ord437');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `register`
--

DROP TABLE IF EXISTS `register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `register` (
  `title` varchar(6) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `gen` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `add` varchar(300) NOT NULL,
  `city` varchar(30) NOT NULL,
  `coun` varchar(30) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `register`
--

LOCK TABLES `register` WRITE;
/*!40000 ALTER TABLE `register` DISABLE KEYS */;
INSERT INTO `register` VALUES ('Mr.','Akash','Jindal','Male','akash.jindal@kelltontech.com','9013628187','Delhi delhi','Delhi','India','01/01/1992','akash123'),('Mr.','Harshit','Gaur','Male','harshit.gaur@kelltontech.com','8178195297','Durga Colony','Kasganj','India','14/03/1991','harshit123'),('Mr.','Rahul','Maheshwari','male','rahul@gmail.com','9458485085','Bilram Gate','Kasganj','India','17-07-1991','rahul123');
/*!40000 ALTER TABLE `register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategory`
--

DROP TABLE IF EXISTS `subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategory` (
  `cat_id` varchar(30) NOT NULL,
  `subcat_id` varchar(30) NOT NULL,
  `subcategory` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
INSERT INTO `subcategory` VALUES ('1','1','Casual Shirts'),('1','2','jeans'),('1','3','T-shirts'),('1','4','Footwear'),('1','5','Shorts'),('1','6','watches'),('2','7','Dresses'),('2','8','Churidar Suits'),('2','9','Kurtas'),('2','10','Sandals'),('2','11','Office Wear'),('2','12','Artificial Jewellery'),('3','13','Baby Apparel'),('3','14','Girls Apparel'),('3','15','Boys Apparel'),('3','16','Kids Toys');
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trash`
--

DROP TABLE IF EXISTS `trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trash` (
  `catg` varchar(50) NOT NULL,
  `subcatg` varchar(50) NOT NULL,
  `itemno` varchar(30) NOT NULL,
  `price` int(30) NOT NULL,
  `desc` varchar(300) NOT NULL,
  `info` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trash`
--

LOCK TABLES `trash` WRITE;
/*!40000 ALTER TABLE `trash` DISABLE KEYS */;
INSERT INTO `trash` VALUES ('1','jeans','a',5454,'ooooo','ooooooo'),('1','jeans','aa',5454,'pppppp','ppppppp');
/*!40000 ALTER TABLE `trash` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-18 11:23:32
