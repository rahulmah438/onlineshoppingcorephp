<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);

include("user.php");


// print_r($_POST);
$title=$_REQUEST['title'];
$fname=$_REQUEST['fname'];
$lname=$_REQUEST['lname'];
$gen=$_REQUEST['gender'];
$email=$_REQUEST['email'];
$phone=$_REQUEST['phone'];
$add=$_REQUEST['address'];
$city=$_REQUEST['city'];
$coun=$_REQUEST['country'];
$dob=$_REQUEST['dob'];
$pass=$_REQUEST['password'];

if($_REQUEST['submit'])
{
	$user->checkUser($title, $fname, $lname, $gen, $email, $phone, $add, $city, $coun, $dob, $pass);
}
?>


<script>
function firstname()
{
  var firstname=/^[a-zA-Z]{4,15}$/;
   if(document.regform.fname.value.search(firstname)==-1)
    {
	 alert("enter correct first name betwwen 4 to 15 characters");
	 document.regform.fname.focus();
	 return false;
	 }
	} 

	function lastname()
{
  var lastname=/^[a-zA-Z]{4,15}$/;
   if(document.regform.lname.value.search(lastname)==-1)
    {
	 alert("enter correct last name betwwen 4 to 15 characters");
	 document.regform.lname.focus();
	 return false;
	 }
	} 

	function Email()
{
 var Email=/^[a-zA-Z0-9-_\.]+@[a-zA-Z]+\.[a-zA-Z]{2,3}$/;
   if(document.regform.email.value.search(Email)==-1)
    {
	 alert("enter correct email");
	 document.regform.email.focus();
	 return false;
	 }
	} 
	
	function phoneno()
	{
	var phoneno=/^[0-9]{10}$/;
  if(document.regform.phone.value.search(phoneno)==-1)
    {
	 alert("enter correct phone no of 10 digits");
	 document.regform.phone.focus();
	 return false;
	 }
	 }
	
	function add()
	{
	var add=/^[a-zA-Z0-9- ]{10,150}$/;
  if(document.regform.address.value.search(add)==-1)
    {
	 alert("enter correct address of at least 10 characters");
	 document.regform.address.focus();
	 return false;
	 }
	 }
	 
	 function cityname()
	 {
	 var cityname=/^[a-zA-Z]{5,30}$/;
	 if(document.regform.city.value.search(cityname)==-1)
    {
	 alert("enter correct city of at least 5 characters");
	 document.regform.city.focus();
	 return false;
	 }
	
	 }
	  function coun()
	 {
	 var coun=/^[a-zA-Z]{5,30}$/;
	 if(document.regform.country.value.search(coun)==-1)
    {
	 alert("enter correct country of at least 5 characters");
	 document.regform.country.focus();
	 return false;
	 }
	
	 }

	 
	function pass()
	{
	var pass=/^[a-zA-Z0-9-_]{6,16}$/;
   if(document.regform.password.value.search(pass)==-1)
    {
	 alert("enter correct pass between 6 to 16 characters");
	 document.regform.password.focus();
	 return false;
	 }
	 }
	
function validate()
{
  	var name=/^[a-zA-Z]{4,15}$/;
   	var email=/^[a-zA-Z0-9-_\.]+@[a-zA-Z]+\.[a-zA-Z]{2,3}$/;
	var phn=/^[0-9]{10}$/;
	var add=/^[a-zA-Z0-9 ]{10,150}$/;
	var residence=/^[a-zA-Z]{5,30}$/;
	var pass=/^[a-zA-Z0-9-_]{6,16}$/;
	  
   if(document.regform.fname.value.search(name)==-1)
    {
	 alert("enter correct first name betwwen 4 to 15 characters");
	 document.regform.fname.focus();
	 return false;
	 }
	 	 
  else if(document.regform.lname.value.search(name)==-1)
    {
	 alert("enter correct last name betwwen 4 to 15 characters");
	 document.regform.lname.focus();
	 return false;
	 }
 
  else if(document.regform.email.value.search(email)==-1)
    {
	 alert("enter correct email");
	 document.regform.email.focus();
	 return false;
	 }
	
  else if(document.regform.phone.value.search(phn)==-1)
    {
	 alert("enter correct phone no of 10 digits");
	 document.regform.phone.focus();
	 return false;
	 }
	 
  else if(document.regform.address.value.search(add)==-1)
    {
	 alert("enter correct address of at least 10 characters");
	 document.regform.address.focus();
	 return false;
	 }
	
  else if(document.regform.city.value.search(residence)==-1)
    {
	 alert("enter correct city of at least 5 characters");
	 document.regform.city.focus();
	 return false;
	 }
	 
  else if(document.regform.country.value.search(residence)==-1)
    {
	 alert("enter correct country of at least 5 characters");
	 document.regform.country.focus();
	 return false;
	 }

	 else if(document.regform.password.value.search(pass)==-1)
    {
	 alert("enter correct pass between 6 to 16 characters");
	 document.regform.password.focus();
	 return false;
	 }
	 
  else 
	{
	return true;
	}
}
	
	 
</script> 


<!DOCTYPE html>

	<head>
			<title> Registration</title>
			<link rel="stylesheet" href="css/a.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>

	<body >

	<div style="width:30%;float:left">
		<br/><br/><br/><br/><br/><br/><br/><br/>
		<img id="register" src="images/7.jpg">
	</div>
	<div class="container">
		<div class="col-md-5" style="margin-left:-230px; float:left;">
		<form method="post" name = "regform" onSubmit="return validate()">
		<h2 align="center">Register yourself</h2>
		<br />
		
		<div class="form-group">
			<label>Title</label>
			<select name = "title" class="form-control">
				<option value="Mr.">Mr.</option>
				<option value="Ms.">Ms.</option>
				<option value="Mrs.">Mrs.</option>
			</select>		
		</div>
		<div class="form-group">
			<label>Enter First Name</label>
			<input type="text" name="fname" placeholder="Enter First Name" onChange="return firstname()" class="form-control"/>
		</div>
		<div class="form-group">
			<label>Enter Last Name</label>
			<input type="text" name="lname" placeholder="Enter Last Name" onChange="return lastname()" class="form-control"/>
		</div>
		<div class="form-group">
			<label>gender</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Male &nbsp;<input type="radio" name="gender" value="Male" checked/>
			Female &nbsp;<input type="radio" name="gender" value="Female" />
			Others &nbsp;<input type="radio" name="gender" value="Others" />
		</div>

		<div class="form-group">
			<label>Enter Email</label>
			<input type="text" name="email" class="form-control" placeholder="Enter your email" onChange="return Email()"/>
		</div>
		<div class="form-group">
			<label>Enter Phone No</label>
			<input type="text" maxlength = "10" name="phone" class="form-control" placeholder="Enter your phone no" onChange="return phoneno()"/>
		</div>
		<div class="form-group">
			<label>Enter Address</label>
			<input type="text" name="address" class="form-control" placeholder="Enter your address" onChange="return add()"/>
		</div>
		<div class="form-group">
			<label>Enter City</label>
			<input type="text" name="city" class="form-control" placeholder="Enter your city" onChange="return cityname()"/>
		</div>
		<div class="form-group">
			<label>Enter Country</label>
			<input type="text" name="country" class="form-control" placeholder="Enter your country" onChange="return coun()"/>
		</div>
		<div class="form-group">
			<label>Enter date of birth</label>
			<input type="text" name="dob" class="form-control" placeholder="Enter your dob in dd-mm-yyyy"/>
		</div>
		<div class="form-group">
			<label>Enter Password</label>
			<input type="password" name="password" class="form-control" placeholder="Enter your password" onChange="return pass()"/>
		</div>
		<div class="form-group" align="center">
			<input type="submit" name="submit" class="btn btn-info" value="Submit" />
			<a href = "index.php"><input type="button" class="btn btn-warning" value="Back" /></a>
		</div>
	
      </form>
    </div>
   </div>
</body>
</html>
