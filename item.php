<?php
include_once("db_config.php");
include_once("interface.php");

class Item extends Database implements ItemMethods {

    public function __construct()  
    {
        $this->db = parent::databaseConnection();
        //print  $this->db;die;
    }
    

    /*** for ordering item get details of items ***/

    public function getItems($itemno){
        $sql = ("select * from items where itemno='$itemno' ");
        $result = mysqli_query($this->db,$sql);
        return $result;
    }

    /*** After filling the order form to place order ***/

    public function placeOrder($pname, $itemno, $price, $size, $uname, $ac_no, $mob_no, $add, $bank, $city, $order_no) 
    {
        $sql = ("insert into orders values('$pname','$itemno','$price','$size','$uname','$ac_no','$mob_no','$add','$bank','$city','$order_no')");
        if (mysqli_query($this->db,$sql) === true) {
        echo "<script>location.href='ordersent.php?order=$order_no'</script>";
        }
        else {
            echo "There is some problem encountered please try after some time";
        }
    }


    /*** for getting the all items of some category and sub-category ***/
    public function getProducts ($category, $subcatg)
    {
        $sql = ("select * from items where category='".$category."' && subcatg='".$subcatg."' ");
        $result = mysqli_query($this->db,$sql);
        return $result;
        
    }
    
    /*** for getting the count of orders ***/
    public function getOrdersCount()
    {
        $sql = ("select count(*) as 'count' from orders");
        $result = mysqli_query($this->db,$sql);
        $row = mysqli_fetch_assoc($result);
        return $row['count'];
    }


    /*** for getting the count of feedbacks ***/
    public function getFeedbacksCount()
    {
        $sql = ("select count(*) as 'count' from feedback");
        $result = mysqli_query($this->db,$sql);
        $row = mysqli_fetch_assoc($result);
        return $row['count'];
    }


    /*** for getting all categories available in database ***/
    public function getCategories()
    {
        $sql = ("select * from category");
        $result = mysqli_query($this->db,$sql);
        return $result;
    }

    /*** for getting all subcategories available in database ***/
    public function getSubCategories()
    {
        $sql = ("select * from subcategory");
        $result = mysqli_query($this->db,$sql);
        return $result;
    }

    /*** for getting all subcategories available in database ***/
    public function insertItem($category, $subcategory, $itemno, $price, $desc, $info)
    {
        $sql = ("insert into items  values('$category','$subcategory','$itemno','$price','$desc','$info')");
        $result = mysqli_query($this->db,$sql);
        return true;
    }


    /*** for viewing all the orders ***/
    public function getOrders()
    {
        $sql = ("select * from orders");
        $result = mysqli_query($this->db,$sql);
        return $result;
    }


    /*** for viewing all the feedbacks ***/
    public function getFeedbacks()
    {
        $sql = ("select * from feedback");
        $result = mysqli_query($this->db,$sql);
        return $result;
    }



    /*** for inserting the product which is going to be deleted ***/
    public function insertIntoTrash($category, $subcategory, $itemno, $price, $desc, $info)
    {
        $sql = ("insert into trash  values('$category','$subcategory','$itemno','$price','$desc','$info')");
        $result = mysqli_query($this->db,$sql);
        return true;
    }


    /*** for deleting the product ***/
    public function DeleteProduct($itemno)
    {
        $sql = ("delete from items where itemno='$itemno'");
        $result = mysqli_query($this->db,$sql);
        return true;
    }

    

    /*** for getting subcategory name ***/
    public function getSubCategoryName($subcategoryId)
    {
        $sql = ("select subcategory from subcategory where subcat_id='$subcategoryId'");
        $result = mysqli_query($this->db,$sql);
        $row = mysqli_fetch_assoc($result);
        return $row['subcategory'];
    }


    /*** for getting a category name by id ***/
    public function getCategoryName($categoryId)
    {
        $sql = ("select * from category where cat_id = '$categoryId' ");
        $result = mysqli_query($this->db,$sql);
        $row = mysqli_fetch_assoc($result);
        return $row['category'];
    }


    /*** for getting a subcategory id by name ***/
    public function getSubCategoryId($subcategory)
    {
        $sql = ("select cat_id from subcategory where subcategory = '$subcategory' ");
        $result = mysqli_query($this->db,$sql);
        $row = mysqli_fetch_assoc($result);
        return $row['cat_id'];
    }


    /*** for updating a product details ***/
    public function updateProduct($categoryId, $subcategory, $itemno, $price, $desc, $info)
    {
        $sql = ("update items set category = '$categoryId', subcatg = '$subcategory',
		price = '$price', desc = '$desc', info = '$info' where itemno = '$itemno' ");
        $result = mysqli_query($this->db,$sql);
        return true;
    }

    
}
$item = new Item();
?>