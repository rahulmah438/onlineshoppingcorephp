<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
session_start();
$name=$_SESSION['AdminName'];
$itemno=$_REQUEST['itemno'];

include __DIR__ . '/../item.php';
  
$result = $item->getItems($itemno);
$ItemDetails = mysqli_fetch_array($result);
$categoryId=$ItemDetails['category'];
$subcategory=$ItemDetails['subcatg'];
$itemno=$ItemDetails['itemno'];
$price=$ItemDetails['price'];
$desc=$ItemDetails['desc'];
$info=$ItemDetails['info'];

$subcategoryId = $item->getSubCategoryId($subcategory);
// $subcategory = $item->getSubCategoryName($subcategoryId);

if($_REQUEST['submit']) {
	$categoryId = $_POST["category"];
	$subcategory = $_POST["subcategory"];
	$price = $_POST["price"];
	$desc = $_POST["textdes"];
	$info = $_POST["textinfo"];
    if($item->updateProduct($categoryId, $subcategory, $itemno, $price, $desc, $info)) {
	    echo "<font size='+2'>item updated successfully</font>";
    }
}
?>
<html>
  <head>
      <link rel="stylesheet" href="css/a.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>
	<div id = "bannerOther">
		<div id="AdminMenuOther"> 
			<a href="?con=17"><span>HOME</span></a>
			<a href="?con=18"><span>ADD ITEM</span></a>
			<a href="?con=21"><span>VIEW ALL</span></a>
			<a href="?con=19"><span>ORDERS ( 
<?php
	$OrdersCount = $item->getOrdersCount();
	echo $OrdersCount;
?>					
			)</span></a>
			<a href="?con=20"><span>FEEDBACKS ( 
<?php
	$count=0;
	$FeedbacksCount = $item->getFeedbacksCount();
	echo $FeedbacksCount;
?>
			)</span></a>
		</div>
	</div><br/><br/>
    <div class="container">
    <div class="col-md-6" style="margin-left:10px; float:left;">
    <center><font color="#00CCFF" size="+3">Update Product</font></center>
	      <form method="post">
          <div class="form-group">
		        <label>Select Category</label>
		          <select name = "category" class="form-control">
			          <option>-- Select category --</option>
      <?php		
        $Categories = $item->getCategories();
        if ($Categories->num_rows > 0 ) {
            while($row = $Categories->fetch_assoc()) {	
				$selectd = "";
				if($row[cat_id] == $categoryId) {
					$selectd = 'selected = selected';
				}			
				echo "<option value=".$row[cat_id]." ".$selectd.">".$row[category]."</option>";
				    }
				} 
				else {
				echo "No results in category";
			  }
			?>					 
		          </select>		
	          </div>  
        
            <div class="form-group">
		        <label>Select Sub Category</label>
		          <select name = "subcategory" class="form-control">
			          <option>-- Select sub category --</option>
      <?php		
        $SubCategories = $item->getSubCategories();
        if ($SubCategories->num_rows > 0 ) {
            while($row = $SubCategories->fetch_assoc()) {				
				$selectd = "";
				if($row[subcategory] == $subcategory) {
					$selectd = 'selected = selected';
				}	
				echo "<option value=".$row[subcat_id]." ".$selectd.">".$row[subcategory]."</option>";
				    }
				} 
				else {
				echo "No results in subcategory";
			  }
			?>					 
		          </select>		
	          </div>  
	          <div class="form-group">
		          <label>Enter Item number</label>
		            <input type="text" readonly="readonly" class="form-control" value = "<?php echo $itemno ?>"/>
	          </div>
  
            <div class="form-group">
		          <label>Enter Price</label>
		            <input type="text" name="price" placeholder="Enter price" class="form-control" value = "<?php echo $price ?>"/>
            </div>
            
            <div class="form-group">
		          <label>Enter Description</label>
		            <input type="text" name="textdes" class="form-control" value = "<?php echo $desc ?>"/>
	          </div>

            <div class="form-group">
		          <label>Enter Info</label>
		            <input type="text" name="textinfo" class="form-control" value = "<?php echo $info ?>"/>
	          </div>
  
            <div class="form-group" align="center">
		          <input type="submit" name="submit" class="btn btn-info" value="Submit" />
		          <a href="?con=17"><input type="button" class="btn btn-warning" value="Back" /></a>
	          </div>
	      </form>
    </div>
   </div>
</body>
</html>



