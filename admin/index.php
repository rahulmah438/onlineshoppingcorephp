<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & E_WARNING);

include __DIR__ . '/../user.php';
// include_once("/../user.php");


if($_REQUEST['submit']) {
  $name=$_REQUEST['name'];
  $pass=$_REQUEST['pass'];
  $login = $user->adminLogin($name, $pass);
}	 
?>
<html>
<head>
<script>
function adminname()
{
    var adminname=/^[a-zA-Z]{4,15}$/;
    if(document.adminform.name.value.search(adminname)==-1) {
      alert("enter correct name");
      document.adminform.name.focus();
      return false;
    }
} 
		
function password()
{
  	var password=/^[a-zA-Z0-9-_]{6,16}$/;
    if(document.adminform.pass.value.search(password)==-1) {
	      alert("enter correct password");
	      document.adminform.pass.focus();
	      return false;
	 }
}
	 
	 	
function validate()
{
    var name=/^[a-zA-Z]{4,15}$/;
    var password=/^[a-zA-Z0-9-_]{6,16}$/;
	  if(document.adminform.name.value.search(name)==-1) {
	      alert("enter correct name");
	      document.adminform.name.focus();
	      return false;
	  }
	  else if(document.adminform.pass.value.search(password)==-1) {
	      alert("enter correct pass");
	      document.adminform.pass.focus();
	      return false;
	 }
	 	else {
	      return true;
	 }
}
	
</script>
      <link rel="stylesheet" href="../css/a.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container"> 
        <div class="col-md-5" style="margin-left:50px; float:left;">
          <form method="post" name="adminform" onSubmit="return validate()" >
          <h2 align="center"> <font size="+1" color="#00CCFF">ADMINISTRATOR LOGIN</font></h2>
          <br />
        <div class="form-group">
          <label>Enter name</label>
          <input type="text" name="name" placeholder="Enter admin name" class="form-control" onChange="return adminname()"/>
        </div>
        <div class="form-group">
          <label>Enter Password</label>
          <input type="password" name="pass" placeholder="Enter password" class="form-control" onChange="return password()"/>
        </div>
        <div class="form-group" align="center">
        <input type="submit" name="submit" class="btn btn-info" value="Submit" />
        <a href = "../index.php"><input type="button" class="btn btn-warning" value="Back" /></a>
      </div>
        </div>
      </form>
    </div>
    </div>
    </
</body>
</html>