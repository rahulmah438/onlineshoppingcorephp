<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & E_WARNING);
include __DIR__ . '/../item.php';
session_start();

$name=$_SESSION['AdminName'];
$category=$_REQUEST['catg'];
$subcategoryId=$_REQUEST['subcatg'];

$subcategory = $item->getSubCategoryName($subcategoryId);
$result = $item->getProducts($category, $subcategory);

if($_REQUEST['delete']) {
	$itemno=$_REQUEST['cb'];
	if($itemno!=NULL)  {
			$flag=0;
			foreach($itemno as $i) {
					$result = $item->getItems($i);
					$arr = mysqli_fetch_array($result);
					$category=$arr['category'];
					$subcategory=$arr['subcatg'];
					$itemno=$arr['itemno'];
					$price=$arr['price'];
					$desc=$arr['desc'];
					$info=$arr['info'];
					
					$result = $item->InsertIntoTrash($category, $subcategory, $itemno, $price, $desc, $info);
					if ($result) {
							$result = $item->DeleteProduct($itemno);
							if ($result) {
									 $flag=1;
							}
					}
			}
			if($flag==1) {
				echo "<font size='+2'>item deleted successfully</font>";
			 }
			 else {
				echo "<font size='+2'>item is not deleted</font>";
			}
	}
	else {
			echo "<font size='+2'>please select a checkbox</font>";
	}
}	

?>
<html>
		<head>
				<link rel="stylesheet" type="text/css" href="css/default.css" title="default">
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		</head>
		<body>
			<div id = "bannerOther">
				<div id="AdminMenuOther"> 
						<a href="?con=17"><span>HOME</span></a>
						<a href="?con=18"><span>ADD ITEM</span></a>
						<a href="?con=21"><span>VIEW ALL</span></a>
						<a href="?con=19"><span>ORDERS ( 
<?php
	$OrdersCount = $item->getOrdersCount();
	echo $OrdersCount;
?>					
					 )</span></a>
					 <a href="?con=20"><span>FEEDBACKS ( 
<?php
	$count=0;
	$FeedbacksCount = $item->getFeedbacksCount();
	echo $FeedbacksCount;
?>
					)</span></a>
			</div>
	</div><br/><br/>
				<center><font color="#00CCFF" size="+3">View Products</font></center>
				<form method='post'>
<?php
if ($result->num_rows > 0 ) {
		// output data of each row
		echo "<table class='table' id='viewTable' cellpadding='10' cellspacing='10' border='1'>
		<tr>
		<th>Product ID</th>
		<th>Price</th>
		<th>Description</th>
		<th>Information</th>
		<th>Action</th>
		<th>Multiple Select</th>
		</tr>
		";
		while($row = $result->fetch_assoc()) {
				echo "<tr>"; 
				echo "<td>" . @$row[itemno] . "</td>";
				echo "<td>" . @$row[price] . "</td>";
				echo "<td>" . @$row[desc] . "</td>";
				echo "<td>" . @$row[info] . "</td>";
				echo "<td><a href= 'index.php?con=23&itemno=$row[itemno]'><input type='button' class='btn btn-default' value='Edit'/></a></td>";
				echo "<td><input name='cb[]' type='checkbox' value='$row[itemno]' /><b>Delete</b></td></tr>";
		}
		echo "</table>";
} 
else {
		echo "0 results";
}	
		echo "<div><center><input class='btn btn-danger' type='submit' name='delete' value='Delete'/>";
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "<a href='?con=17'><input type='button' class='btn btn-warning' value='Back' /></a></center></div>";

?>
				</form>
		</body>
</html>