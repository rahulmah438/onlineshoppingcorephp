<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & E_WARNING);
include __DIR__ . '/../item.php';
session_start();
$name=$_SESSION['AdminName'];

$category=$_REQUEST['category'];
$subcategoryId=$_REQUEST['subcategory'];
if($_REQUEST['submit'])
{
 echo "<script>location.href='index.php?con=22&catg=$category&subcatg=$subcategoryId'</script>";
}

  
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/default.css" title="default">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div id = "bannerOther">
			<div id="AdminMenuOther"> 
				<a href="?con=17"><span>HOME</span></a>
				<a href="?con=18"><span>ADD ITEM</span></a>
				<a href="?con=21"><span>VIEW ALL</span></a>
				<a href="?con=19"><span>ORDERS ( 
<?php
	$OrdersCount = $item->getOrdersCount();
	echo $OrdersCount;
?>					
				)</span></a>
				<a href="?con=20"><span>FEEDBACKS ( 
<?php
	$count=0;
	$FeedbacksCount = $item->getFeedbacksCount();
	echo $FeedbacksCount;
?>
				)</span></a>
		</div>
	</div><br/><br/>
	<div class="container">
    <div class="col-md-6" style="margin-left:10px; float:left;">
    <center><font color="#00CCFF" size="+3">View Products</font></center>
	      <form method="post">
          <div class="form-group">
		        <label>Select Category</label>
		          <select name = "category" class="form-control">
			          <option selected>-- Select category --</option>
      <?php		
        $Categories = $item->getCategories();
        if ($Categories->num_rows > 0 ) {
            while($row = $Categories->fetch_assoc()) {				
				echo "<option value=".$row[cat_id].">".$row[category]."</option>";
				    }
				} 
				else {
				echo "No results in category";
			  }
			?>					 
		        </select>		
	        </div>  
        
            <div class="form-group">
		        <label>Select Sub Category</label>
		          <select name = "subcategory" class="form-control">
			          <option selected>-- Select sub category --</option>
      <?php		
        $SubCategories = $item->getSubCategories();
        if ($SubCategories->num_rows > 0 ) {
            while($row = $SubCategories->fetch_assoc()) {				
				echo "<option value=".$row[subcat_id].">".$row[subcategory]."</option>";
				    }
				} 
				else {
				echo "No results in subcategory";
			  }
			?>	
				</select>		
	        </div>  				 
			<div class="form-group" align="center">
		        <input type="submit" name="submit" class="btn btn-info" value="Submit" />
		        <a href="?con=17"><input type="button" class="btn btn-warning" value="Back" /></a>
	        </div>
	    </form>
    </div>
   </div>
</body>
</html>

	
	
	