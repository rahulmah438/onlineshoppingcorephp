<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & E_WARNING);

if(isset($_POST["delete"])) {  
    $id = str_replace( '/', '', $_POST["hidden_id"] );
    foreach($_SESSION["shopping_cart"] as $keys => $values) {  
        if($values["item_id"] == $id)  {  
            unset($_SESSION["shopping_cart"][$keys]);  
            echo '<script>alert("Item Removed")</script>';  
            echo("<script>window.location = 'index.php?con=24';</script>");
        }  
    }  
}

?>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/default.css" title="default">
    <style>
    .container{padding: 50px;}
    input[type="number"]{width: 20%;}
    </style>
</head>
<body>
<div style="text-align:center;"><h2><font size="+3" color="#00CCFF">Cart</font></h2></div>
<div class="container">
    <table id="carttable" class="table" style="max-width:70%;margin-left:-50px;text-align:center;">
    <thead>
        <tr>
            <th >Item Id</th>  
            <th >Price</th>  
            <th >Quantity</th>  
	       <th >Total</th>  
	       <th >Action</th>  
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php                        
                if(!empty($_SESSION["shopping_cart"])) {  
                    $total = 0;  
                    foreach($_SESSION["shopping_cart"] as $keys => $values) {  
            ?>  
            <tr>  
                <form method="post">
                <td><?php echo $values["item_id"]; ?></td>  
                <td>Rs <?php echo $values["item_price"]; ?></td>  
                <td><?php echo $values["item_quantity"]; ?></td>  
                <td>Rs <?php echo number_format($values["item_quantity"] * $values["item_price"], 2); ?></td>  
                <input type='hidden' name='hidden_id' value=<?php echo $values["item_id"]; ?>/>
                <td><input type='submit' name='delete' style='margin-top:5px;' 
                class='btn btn-danger' value='Remove' /></td>
            </tr>  
                <?php  
                        $total = $total + ($values["item_quantity"] * $values["item_price"]);  
                    }  
                ?>  
                <tr>  
                    <td colspan="3" align="right"><b>Total</b></td>  
                    <td align="right"><b>Rs <?php echo number_format($total, 2); ?></b></td>  
                    <td></td>  
                </tr>  
                <?php  
                }  
                ?>  
                <tr></tr>
                <tfoot>
                    <tr>
                        <td><a href="index.php" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Continue Shopping</a></td>
                        <td colspan="3"></td>
                        <td><a href="index.php?con=12" class="btn btn-success btn-block">Checkout <i class="glyphicon glyphicon-menu-right"></i></a></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </body>
</html>
