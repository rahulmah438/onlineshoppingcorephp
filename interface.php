<?php
interface UserMethods { 
    public function checkUser($title, $fname, $lname, $gen, $email, $phone, $add, $city, $coun, $dob, $pass);
    public function login($email, $pass, $item);
    public function submitFeedback($name, $phone, $email, $subj, $msg);
    public function adminLogin($name, $pass);
}
interface ItemMethods { 
    public function getItems($itemno);
    public function placeOrder($pname, $itemno, $price, $size, $uname, $ac_no, $mob_no, $add, $bank, $city, $order_no);
    public function getProducts ($category, $subcatg);
    public function getOrdersCount();
    public function getFeedbacksCount();
    public function getCategories();
    public function getSubCategories();
    public function insertItem($category, $subcategory, $itemno, $price, $desc, $info);
    public function getOrders();
    public function insertIntoTrash($category, $subcategory, $itemno, $price, $desc, $info);
    public function DeleteProduct($itemno);
}
?>