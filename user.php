<?php
include_once("db_config.php");
include_once("interface.php");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

class User extends Database implements UserMethods {

    public function __construct()  
    {
        $this->db = parent::databaseConnection();
        //print  $this->db;die;
    }
    

    /*** for checking user in the database if not then register user ***/

    public function checkUser($title, $fname, $lname, $gen, $email, $phone, $add, $city, $coun, $dob, $pass){
        $sql = ("select email from register where email='$email' ");
        
        //checking if the email is already registered
        $result = mysqli_query($this->db,$sql);
        $UserDetails = mysqli_fetch_array($result);

        if($UserDetails['email']!=$email) {
            $sql = "insert into register values('$title','$fname','$lname','$gen','$email','$phone','$add','$city','$coun','$dob' ,'$pass')";
            mysqli_query($this->db,$sql);
            echo "<script>location.href='index.php?con=13 & wel=$email'</script>";
        }
        else {
            echo "user already exists";
        }
    }
    
    
    
    /*** for user login process ***/

    public function login($email, $pass, $itemno){
        $sql = ("select * from register where email='$email' ");
        
        //checking if the email is already registered
        $result = mysqli_query($this->db,$sql);
        $UserDetails = mysqli_fetch_assoc($result);
        if(($UserDetails['email']==$email) && ( $UserDetails['pass']==$pass))  {
            $_SESSION['UserEmail']=$email;
            $_SESSION['title'] = $UserDetails["title"];
            $_SESSION['firstname'] = $UserDetails["fname"];
            $_SESSION['lastname'] = $UserDetails["lname"];
            session_start();          
            echo "<script>location.href='index.php?con=15 & itemno=$itemno'</script>";
        }
        else {
            echo "<font face='Lucida Handwriting' color='red' size='+2'>id and password do not match</font>";
            return false;
        }
    }
        

    /*** for submit the feedback by any user ***/
    public function submitFeedback($name, $phone, $email, $subj, $msg) 
    {
        $sql = "insert into feedback values('$name','$phone','$email','$subj','$msg')";
        if (mysqli_query($this->db,$sql) === true) {
            echo "<font face='Lucida Handwriting' color='red' size='+1'>Message sent successfully</font>";
        }
    }


    /*** for admin login process ***/

    public function adminLogin($name, $pass){
        $sql = ("select * from details where name = '".$name."' "); 
        
        $result = mysqli_query($this->db,$sql);
        $AdminDetails = mysqli_fetch_array($result);
        if(($AdminDetails['name']==$name) && ( $AdminDetails['pass']==$pass))  {
            $_SESSION['AdminName']=$name;
            session_start();          
            echo "<script>location.href='index.php?con=17'</script>";
        }
        else {
            echo "<font face='Lucida Handwriting' color='red' size='+2'>id and password do not match</font>";
            return false;
        }
    }
}
$user = new User();
?>