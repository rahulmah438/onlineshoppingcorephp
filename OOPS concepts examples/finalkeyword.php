<?php
final class Inc {
	
	function postinc($php_count) {
	return $php_count++;
	}
}

class SubInc extends Inc {
	function preinc($php_count) {
	return ++$php_count;
	}
}
?>

O/p => Fatal error: Class SubInc may not inherit from final class (Inc) in ... on line ...


<?php
class Inc {
	final public static function postinc($php_count) {
	return $php_count++;
	}
}

class SubInc extends Inc{
	public static function postinc($php_count) {
	return $php_count++;
	}
	public static function preinc($php_count) {
	return ++$php_count;
	}
}
?>
O/p => Cannot override final method Inc::postinc() ... on line ...
